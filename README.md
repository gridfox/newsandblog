##Node.js project.

This project is a CMS for News and Blog. User will be able to register and post their own blog or article. People can read blog or news as a guest or registered user. They can like or dislike a news or blog to show their expression or opinion of the article or news. Blog will have the commenting functionality. News can be shared in other website like social media sites. 

use "npm install" to install the dependency for this project.

use "node app.js" to run the application.

goto http://localhost:5500/ to browser.