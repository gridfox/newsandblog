const Sequelize = require("sequelize");
const db = require("../config/database");

const Users = db.define("users", {
  user_id: {
    type: Sequelize.INTEGER
  },
  firstName: {
    type: Sequelize.STRING
  },
  lastName: {
    type: Sequelize.STRING
  },
  email: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
  },
  is_admin: {
    type: Sequelize.BOOLEAN
  },
  activated: {
    type: Sequelize.BOOLEAN
  },
  disabled: {
    type: Sequelize.BOOLEAN
  }
});

module.exports = Users;
