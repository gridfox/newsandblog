const express = require("express");
const path = require("path");
const exphbs = require("express-handlebars");
const bodyParser = require("body-parser");
const app = express();

//database
const db = require("./config/database");
db.authenticate()
  .then(() => console.log("Database connected"))
  .catch(err => console.log("Error: " + err));

//setting views folder and templating engine
app.set("views", path.join(__dirname, "/views/"));
app.engine(
  "hbs",
  exphbs({
    extname: "hbs",
    defaultLayout: "mainLayout",
    layoutsDir: __dirname + "/views/layouts/"
  })
);
app.set("view engine", "hbs");

// Body Parser
app.use(bodyParser.urlencoded({ extended: false }));

// Set static folder
app.use(express.static(path.join(__dirname, "public")));

//index
app.use("/", require("./routes/index"));

//register
app.use("/register", require("./routes/register"));

// //new portal
app.use("/", require("./routes/newsportal"));

// //blog
app.use("/", require("./routes/blogportal"));

// //users
// app.use('/',require('./routes/user'));

// //category
// app.use('/',require('./routes/category'));

// //search
// app.use('/',require('./routes/search'));

//server information and configuration
const port = 5500;

app.listen(port, () => console.log(`Server started at ${port}`));
