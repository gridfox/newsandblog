const express = require('express');
const app = express();
const router = express.Router();

router.get('/news',(req,res)=>{
    res.render('newsportal',{ 
        title: 'News page'
    })
})

module.exports = router;