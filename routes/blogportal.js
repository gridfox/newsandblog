const express = require('express');
const app = express();
const router = express.Router();

router.get('/blog',(req,res)=>{
    res.render('blogportal',{       
        title: 'Blog page'
    })
})

module.exports = router;