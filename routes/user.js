const express = require("express");
const app = express();
const router = express.Router();

router.get("/users", (req, res) => {
  res.render("user", {
    title: "Blog page"
  });
});

module.exports = router;
